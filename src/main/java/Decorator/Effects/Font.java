package Decorator.Effects;

import Decorator.EffectInterfaceDecorator;
import Decorator.Factories.FontFactory;

public class Font extends EffectInterfaceDecorator {
    private String fontType;

    public Font(EffectInterface effect, String fontType){
        super(effect);
        this.fontType = fontType;
    }
    public void applyEffect() {
        super.applyEffect();
        FontInterface fontInterface = FontFactory.getFont(fontType);
        fontInterface.applyFont();
    }

}
