package Decorator.Fonts;

import Decorator.Effects.FontInterface;

public class TimesNewRoman implements FontInterface {

    public TimesNewRoman() {
        super();
    }

    @Override
    public void applyFont() {
        System.out.println("Font: Times New Roman");
    }
}
