package Decorator.Effects;

import Decorator.EffectInterfaceDecorator;

public class Signature extends EffectInterfaceDecorator {
    private String name;
    public Signature(String name, EffectInterface effect){
        super(effect);
        this.name = name;
    }
    @Override
    public void applyEffect(){
        System.out.println("Name: " + this.name);
    }
}
