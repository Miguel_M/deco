import Decorator.Effects.Cropping;
import Decorator.Effects.EffectInterface;
import Decorator.Effects.Font;
import Decorator.Effects.Zoom;
import Decorator.Effects.Signature;


public class Main {
    public static void main(String... args) {

        EffectInterface effect = new Signature("Bob",null);
        effect.applyEffect();
        System.out.println("\n---------");
        EffectInterface cropping = new Cropping(effect);
        cropping.applyEffect();
        System.out.println("\n---------");
        EffectInterface zoom = new Zoom(new Cropping(effect),"in");
        zoom.applyEffect();
        System.out.println("\n---------");
        EffectInterface font = new Font(new Zoom(new Cropping(effect),"out"),"TimesNewRoman");
        font.applyEffect();
        System.out.println("\n---------");
        EffectInterface font2 = new Font(new Cropping(new Zoom (effect,"in")),"Arial");
        font2.applyEffect();
    }

}