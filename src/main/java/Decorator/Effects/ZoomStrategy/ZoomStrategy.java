package Decorator.Effects.ZoomStrategy;

public interface ZoomStrategy {
    void applyZoom();
}
