package Decorator.Fonts;

import Decorator.Effects.FontInterface;

public class ComicSans implements FontInterface {

    public ComicSans() {
        super();
    }

    @Override
    public void applyFont() {
        System.out.println("Font: ComicSans");
    }
}
