package Decorator.Effects.ZoomStrategy;

public class ZoomOut implements ZoomStrategy {
    @Override
    public void applyZoom() {
        System.out.println("Zoom Out: True");
    }
}
