package Decorator.Factories;

import Decorator.EffectInterfaceDecorator;
import Decorator.Effects.FontInterface;
import Decorator.Fonts.Arial;
import Decorator.Fonts.ComicSans;
import Decorator.Fonts.TimesNewRoman;

public class FontFactory extends EffectInterfaceDecorator {

    final String fontName;

    FontFactory(EffectInterfaceDecorator effectDecorator, String fontName){
        super(effectDecorator);
        this.fontName = fontName;
    }

    @Override
    public void applyEffect() {
        super.applyEffect();
        getFont(fontName);

    }
    public static FontInterface getFont(String fontType) {
        FontInterface fontInterface;
        switch (fontType){
            case "Arial":
                fontInterface = new Arial();
                break;
            case "ComicSans":
                fontInterface = new ComicSans();
                break;
            case "TimesNewRoman":
                fontInterface = new TimesNewRoman();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + fontType);
        }
        return fontInterface;
    }
}
