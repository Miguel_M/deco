package Decorator.Effects;

import Decorator.EffectInterfaceDecorator;
import Decorator.Effects.ZoomStrategy.ZoomIn;
import Decorator.Effects.ZoomStrategy.ZoomOut;
import Decorator.Effects.ZoomStrategy.ZoomStrategy;

public class Zoom extends EffectInterfaceDecorator {
    private String zoomType;

    public Zoom(EffectInterface effect, String zoomType){
        super(effect);
        this.zoomType=zoomType;
    }

    public void applyEffect() {
        super.applyEffect();
        ZoomStrategy zoom = getStrategy(zoomType);
        zoom.applyZoom();
    }

    private static ZoomStrategy getStrategy(String zoomType) {
        ZoomStrategy zoom;
        switch (zoomType){
            case "in":
                zoom = new ZoomIn();
                break;
            case "out":
                zoom = new ZoomOut();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + zoomType);
        }
        return zoom;
    }
}
