package Decorator.Fonts;

import Decorator.Effects.FontInterface;

public class Arial implements FontInterface {

    public Arial() {
        super();
    }

    public void applyFont() {
        System.out.println("Font: Arial ");
    }
}
