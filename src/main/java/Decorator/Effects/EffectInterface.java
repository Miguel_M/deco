package Decorator.Effects;

public interface EffectInterface {
    void applyEffect();
}
