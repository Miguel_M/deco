package Decorator.Effects.ZoomStrategy;

public class ZoomIn implements ZoomStrategy {

    @Override
    public void applyZoom() {
        System.out.println("Zoom In: True");
    }
}
