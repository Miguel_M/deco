package Decorator;

import Decorator.Effects.EffectInterface;

public class EffectInterfaceDecorator implements EffectInterface {
    private final EffectInterface effect;

    public EffectInterfaceDecorator(EffectInterface effect) {
        this.effect = effect;
    }

    @Override
    public void applyEffect() {
        this.effect.applyEffect();

    }
}
