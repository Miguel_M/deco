package Decorator.Effects;

import Decorator.EffectInterfaceDecorator;

public class Cropping extends EffectInterfaceDecorator {
    public Cropping(EffectInterface effect){
        super(effect);
    }
    public void applyEffect() {
        super.applyEffect();
        System.out.println("Crop: True");
    }
}
