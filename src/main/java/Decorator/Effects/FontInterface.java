package Decorator.Effects;

public interface FontInterface {
    void applyFont();
}
